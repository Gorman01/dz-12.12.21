public class ArrayIntLABA {
    private static void printA(IntArrayList l) {
        for (int i = 0; i < l.size(); i++) {
            int n = l.get(i);
            System.out.print(n + " ");
        }
        System.out.println();
        System.out.println(l.size());
    }

    public static void main(String[] args) {
        IntArrayList l = new IntArrayList();
        l.add(12);
        for (int i = 1; i < 10; i++) {
            l.add(i);
        }
        printA(l);

        for (int i = 0; i < 7; i++) {
            l.remove();
        }

        printA(l);


    }

}


/**
 * Написать собственную реализацию класса ArrayList, но для работы со значениями
 * типа int (не Integer). Наследовать от чего либо не нужно.
 * Ваш класс будет хранить элементы в массиве (будет иметь поле private int[] values)
 * Методы, которые потребуются реализовать:
 * add
 * get
 * size
 * remove
 */