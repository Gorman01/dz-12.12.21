public class IntArrayList {

    private int[] storage = new int[8];
    private int currentIndex = 0;

    public void add(int item) {
        if (currentIndex > (storage.length - 1)) {
            int[] temp = new int[storage.length * 2];
            System.arraycopy(storage, 0, temp, 0, storage.length);
            storage = temp;
        }
        storage[currentIndex++] = item;
    }

    public int get(int index) {
        if (index >= size() || index < 0)
            throw new IllegalArgumentException("Индекс находится за пределами диапазона списка");
        return storage[index];
    }

    public int size() {
        return currentIndex;
    }

    public void remove() {
        if (size() == 0) throw new IllegalStateException("нечего удалить");
        if (storage.length / size() == 2) {
            int[] temp = new int[storage.length / 2];
            System.arraycopy(storage, 0, temp, 0, size());
            storage = temp;
        }
        currentIndex--;
    }
}
